"""
T.Sarvar
03/01/2024

'requests' MODULI BILAN ISHLASH
=> tashqi veb sahifalar bilan ishlash
"""
import googletrans
import requests
from bs4 import BeautifulSoup
from wordcloud import WordCloud
import matplotlib.pyplot as plt
from pprint import pprint


sahifa = 'https://kun.uz/news/main'
r = requests.get(sahifa)
# pprint(r.text)
soup = BeautifulSoup(r.text, 'html.parser')
news = soup.find_all(class_="news-title")

for i, yangilik in enumerate(news):
    print(f"{i + 1}-yangilik: {yangilik.text}")

matn = 's'
for n in news:
    matn += n.text

stopwords = ["учун", "лекин", "билан", "ва", "бор", "хил", "йил"]
wordcloud = WordCloud(width=1000, height=1000,
                      background_color='white',
                      stopwords=stopwords,
                      min_font_size=20).generate(matn)

plt.figure(figsize = (8, 8), facecolor=None)
plt.imshow(wordcloud)
plt.axis("off")
plt.tight_layout(pad = 0)
plt.show()





# country = 'Uzbekistan'
# url = f"https://restcountries.eu/rest/v2/name/{country}"
# r = requests.get(url)
# r_json = r.json()[0]
# print(r_json)
# print(r_json.keys())
# print(r_json['capital'])







# MASLAHATLAR OLISH UCHUN API

# url = "https://api.adviceslip.com/advice"
# r = requests.get(url)
# advice = r.json()['slip']['advice']
# print(advice)
#
# translator = googletrans.Translator()
# tarjima = translator.translate(advice, dest='uz')
# print(tarjima.text)
