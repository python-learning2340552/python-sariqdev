"""
T.Sarvar
03/01/2024

'googletrans' MODULI BILAN ISHLASH
=> tarjima qilish
"""

from googletrans import Translator
tarjimon = Translator()

matn_uz = "Python - dunyodagi eng mashxur dasturlash tili"

tarjima = tarjimon.translate(matn_uz)  # default ingliz tiliga tarjima qiladi
# print(tarjima.text)  # tarjima qilingan text
# print(tarjima.src)   # original matn tili
# print(tarjima.dest)  # tarjima qilingan matn tili

tarjima_ru = tarjimon.translate(matn_uz, dest='ru')
print(tarjima_ru.text)


# Ingliz tilidan tarjima
matn_en = "Tashkent is the capital of Uzbekistan"
tarjima_uz = tarjimon.translate(matn_en, dest='uz')
print(tarjima_uz.text)
