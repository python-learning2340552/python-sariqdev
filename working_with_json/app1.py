"""
JSON bilan ishlash
"""

import json

x = 10
x_json = json.dumps(x)

y = 5.5
y_json = json.dumps(y)

m = True
m_json = json.dumps(m)

ism = 'Anvar'
ism_json = json.dumps(ism)
familya_json = json.dumps('Narzullayev')

sonlar = (12, 45, 23, 67)
sonlar_json = json.dumps(sonlar)

# print(sonlar_json)

bemor = {
    "ism": "Javlon Hakimov",
    "yosh": 28,
    "oilali": True,
    "farzandlar": ("Anvar", "Bonu"),
    "allergiya": None,
    "dorilar": [
        {"nomi": "Analgin", "miqdori": 0.5},
        {"nomi": "Panadol", "miqdori": 1.2}
    ]
}

bemor_json = json.dumps(bemor, indent=4)  # indent => o'qishga qulay usulga o'tkazib beradi
print(bemor.keys())
print(bemor['dorilar'])
print(bemor_json)

bemor2 = json.loads(bemor_json)
print(type(bemor2))
print(bemor2)



# WRITE TO JSON FILE
with open('bemor.json', 'w') as file:
    json.dump(bemor, file)

# READ FROM JSON FILE
with open('bemor.json') as file:
    b = json.load(file)

print(type(b))
print(b)
