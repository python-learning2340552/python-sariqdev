"""
CLass, objectlar va ular o'rtasidagi bog'lanishlar
"""


class Talaba:
    def __init__(self, ism, familya, tyil):
        self.ism = ism
        self.familya = familya
        self.tyil = tyil
        self.bosqich = 1

    def get_name(self):
        return self.ism

    def get_lastname(self):
        return self.familya

    def get_fullname(self):
        return f'{self.get_name()} {self.get_lastname()}'

    def get_age(self, yil):
        return yil - self.tyil

    def set_bosqich(self, yangi_bosqich):
        self.bosqich = yangi_bosqich

    def update_bosqich(self):
        self.bosqich += 1

    def tanishtir(self):
        print(f"Ismim {self.ism} {self.familya}, tug'ilgan yilim {self.tyil}")

    def get_info(self):
        return f"{self.ism} + {self.familya}. {self.bosqich}-boshqich talabasi"


class Fan:
    def __init__(self, nomi):
        self.nomi = nomi
        self.talabalar_soni = 0
        self.talabalar = []

    def add_student(self, talaba):
        self.talabalar.append(talaba)
        self.talabalar_soni += 1

    def get_name(self):
        return self.nomi

    def get_students(self):
        return [talaba.get_fullname() for talaba in self.talabalar]

    def get_students_num(self):
        return self.talabalar_soni


matematika = Fan("Oliy Matematika")
talaba1 = Talaba("Alijon", "Valiyev", 2000)
talaba2 = Talaba("Hasan", "Mamsodirov", 2001)
talaba3 = Talaba("Akrom", "Bo'riyev", 2000)
matematika.add_student(talaba1)
matematika.add_student(talaba2)
matematika.add_student(talaba3)

print(matematika.get_students())


def see_methods(klass):  # dir(class/object) => class/object haqida umumiy ma'lumotlar beradi
    return [method for method in dir(klass) if method.startswith('__') is False]


print(dir(Talaba))  # class
print(dir(talaba1))  # object
print(see_methods(Talaba))
