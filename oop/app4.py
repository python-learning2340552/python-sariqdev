"""
DUNDER METHODS
"""


class Avto:
    __num_avto = 0  # classga tegishli o'zgaruvchi

    def __init__(self, make, model, rang, yil, narx, km=0):
        self.make = make
        self.model = model
        self.rang = rang
        self.yil = yil
        self.narx = narx
        Avto.__num_avto += 1

    def __str__(self):  # toString function in Java
        return f"Avto: {self.make} {self.model}"

    # SUGGESTED
    def __repr__(self):  # toString function in Java, same as above
        return f"Avto: {self.make} {self.model}"

    # COMPARISON OPERATORS
    ##################################
    def __eq__(self, other):
        return self.narx == other.narx

    def __lt__(self, other):
        return self.narx < other.narx

    def __le__(self, other):
        return self.narx <= other.narx
    ##################################


class AvtoSalon:
    def __init__(self, name):
        self.name = name
        self.avtolar = []

    def __repr__(self):
        return f"{self.name} avtosaloni"

    # Array kabi murojaat qilish imkonini beradi
    ############################################
    def __getitem__(self, index):
        return self.avtolar[index]

    def __setitem__(self, index, qiymat):
        self.avtolar[index] = qiymat

    ############################################

    def __call__(self, *qiymat):  # obyektga funksiya ko'rinishida murojaat qilish imkoniyati yaratish
        if qiymat:
            for avto in qiymat:
                self.add_avto(avto)
        else:
            return [avto for avto in self.avtolar]

    def __add__(self, other):
        if isinstance(other, AvtoSalon):
            yangi_salon = AvtoSalon(f"{self.name} {other.name}")
            yangi_salon.avtolar = self.avtolar + other.avtolar
            return yangi_salon
        elif isinstance(other, Avto):
            self.add_avto(other)

    def add_avto(self, *qiymat):
        for avto in qiymat:
            if isinstance(avto, Avto):
                self.avtolar.append(avto)
            else:
                print("Avto kiriting")


salon1 = AvtoSalon("MaxAvto")
salon2 = AvtoSalon("Avto Lider")

avto1 = Avto("GM", "Malibu", "Qora", 2020, 40000)
avto2 = Avto("GM", "Gentra", "Oq", 2020, 20000)
avto3 = Avto("Toyota", "Carolla", "Silver", 2018, 45000)
salon1.add_avto(avto1, avto2, avto3)

# print(salon1[1])
# salon1[1] = Avto("BMW", "x7", "Qora", 2019, 70000)
# print(salon1[:])

avto4 = Avto("Mazda", "6", "Qizil", 2015, 35000)
avto5 = Avto("Volkswagen", "Polo", "Qora", 2015, 30000)
avto6 = Avto("Honda", "Accord", "Oq", 2017, 42000)
salon2(avto4, avto5, avto6)

# salon1(avto4, avto5)  # __call_ method
# print(salon1())  # bu imkoniyatlarni yaratish uchun yuqorida __call__ methodi yaratilgan

salon3 = salon1 + salon2  # __add__ method
print(salon3())  # __call_ method
salon1 + avto4  # __add__ method
print(salon1())  # __call_ method