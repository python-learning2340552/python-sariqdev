"""
T.Sarvar
03/01/2024

XATOLAR BILAN ISHLASH
"""

# ValueError
yosh = input("Yoshingizni kiriting: ")
try:
    yosh = int(yosh)
except ValueError:
    print("Siz butun son kiritmadizngiz !!!")
else:
    print(f"Siz {2024 - yosh}-yilda tug'ilgansiz")


# ZeroDivisionError
x, y = 5, 10
try:
    y/(x - 5)
except ZeroDivisionError:
    print("0 ga bo'lib bo'lmaydi !!!")

# IndexError
mevalar = ['olma', 'anor', 'anjir', 'uzum']
try:
    print(mevalar[5])
except IndexError:
    print(f"Ro'yxatda {len(mevalar)} ta meva bor xolos !!!")
    