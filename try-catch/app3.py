"""
T.Sarvar
03/01/2024

XATOLAR BILAN ISHLASH
"""

x = input("Son kiriting: ")
try:
    x = int(x)
    y = 20/x
except ValueError:
    print("Butun son kiritmadingiz !!!")
except ZeroDivisionError:
    print("0 ga bo'lish mumkin emas !!!")
else:
    print(f"x = {x}")
