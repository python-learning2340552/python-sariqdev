"""
T.Sarvar
02/01/2024

FAYLLAR BILAN ISHLASH
FAYLDAN MA'LUMOT O'QISH
"""

# 1) NOT RECOMMENDED WAY

# file = open("pi.txt")
# PI = file.read()
# print(PI)
# file.close()



# 2) RECOMMENDED WAY

# with open("pi.txt") as file:  # close the file automatically
#     pi = file.read()
#
# print(pi)
#
# pi = pi.rstrip()
# pi = pi.replace('\n', '')
# pi = float(pi)
# print(pi)


# READ THE FILE LINE BY LINE

filename = '../data/talabalar.txt'
# with open(filename) as file:
#     for line in file:
#         print(line)

# READ ALL LINES TO LIST
with open(filename) as file:
    talabalar = file.readlines()

print(talabalar)
talabalar = [talaba.rstrip() for talaba in talabalar]
print(talabalar)