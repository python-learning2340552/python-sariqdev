"""
T.Sarvar
02/01/2024

FAYLLAR BILAN ISHLASH
FAYLGA MA'LUMOT YOZISH
"""

filename = 'new_file.txt'
ism = 'Olimjon Hasanov'
tyil = 2004
with open(filename, 'w') as file:  # old data inside the file is deleted if it exists
    file.write(ism + '\n')
    file.write(str(tyil) + '\n')

with open(filename, 'a') as file:  # new data is appended inside the file
    file.write('Alijon Valiyev\n')
    file.write('2000')
    