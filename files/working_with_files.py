"""
T.Sarvar
02/01/2024

FAYLLAR BILAN ISHLASH
FAYLGA O'ZGARUVCHILARNI, OBYEKTLARNI,.... YOZISH
"""

import pickle
from oop.avto import Avto

talaba1 = {'ism': 'Hasan', 'familya': 'Husanov', 'tyil': 2003, 'kurs': 2}
talaba2 = {'ism': 'Alijon', 'familya': 'Valiyev', 'tyil': 2002, 'kurs': 1}

with open('info', 'wb') as file:  # wb = write binary
    pickle.dump(Avto(make='GM', model='Malibu', rang='Qora', yil=2021, narx=35000, km=750), file)
    pickle.dump(talaba1, file)
    pickle.dump(talaba2, file)


with open('info', 'rb') as file:  # rb = read binary
    avto = pickle.load(file)
    t1 = pickle.load(file)
    t2 = pickle.load(file)

print(avto)
print(t1)
print(t2)