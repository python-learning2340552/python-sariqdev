"""
T.Sarvar
03/01/2024

'MATH' KUTUBXONASI BILAN ISHLASH
"""

import math
PI = math.pi
print(f"PI ning qiymati: {PI}")
E = math.e
print(f"e ning qiymati: {E}")


# trigonometriya
# print(math.sin(math.pi/2))
# print(math.cos(0))
# print(math.tan(PI))


# radian va burchaklar
# print(math.degrees(math.pi/3))
# print(math.radians(90))


# logarifmlar
# print(math.log(25, 5))
# print(math.log10(1000))

# sonlarni yaxlitlash
# x = 4.3
# print(math.ceil(x))   # katta butun sonni qaytaradi
# print(math.floor(x))  # kichik butun sonni qaytaradi


# Kvadrat ildiz va darajalar bilan ishlash
x = 9
print(math.sqrt(x))
print(math.pow(x, 2))
print(math.pow(x, 1/2))
