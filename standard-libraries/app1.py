"""
T.Sarvar
03/01/2024

'DATETIME' KUTUBXONASI BILAN ISHLASH
"""

import datetime as dt

# hozir = dt.datetime.now()
# print(hozir)
# print(hozir.date())   # kun
# print(hozir.time())   # vaqt
# print(hozir.year)     # yil
# print(hozir.month)    # oy
# print(hozir.day)      # kun
# print(hozir.hour)     # soat
# print(hozir.minute)   # minut
# print(hozir.second)   # sekund

# bugun = dt.date.today()
# print(f"Bugungi sana: {bugun}")
# ertaga = dt.date(2024, 1, 4)
# print(f"Ertangi sana: {ertaga}")


# sanalar o'rtasidagi farq
# bugun = dt.date.today()
# keyin = dt.date(2024, 2, 3)
# farq = keyin - bugun
# print(f"Kunlar orasida {farq.days} kun bor")

# soatlar orasidagi farq
hozir = dt.datetime.now()
futbol_time = dt.datetime(2024, 1, 6, 23, 45, 0)
farq = futbol_time - hozir
sekundlar = farq.seconds
minutlar = int(sekundlar/60)
soatlar = int(minutlar/60)
print(f"Futbol boshlanishiga {farq.days} kunu {soatlar} soat qoldi")
